# ProvaIdevs1

**Aluno:** Vinicius Dalsasso

## Cadastro de computadores

Programa para cadastro de computadores, registrando dados tais como processador, quantidades de memória RAM e armazenamento, e preço.

## Edição e exclusão

Edite e exclua os registros de computadores.

## Página de estatísticas

Além de incluir e remover computadores, é possível visualizar dados sobre a quantidade de computadores, valor total em estoque, e menores e maiores valores entre os computadores.
