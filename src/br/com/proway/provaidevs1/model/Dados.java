package br.com.proway.provaidevs1.model;

import java.util.ArrayList;

public class Dados {

    public ArrayList<Computador> listaComputador = new ArrayList<>();

    public void inserirComputador(Computador computador) {
        listaComputador.add(computador);
    }

    public void deletarComputador(int id) {
        listaComputador.removeIf(t -> t.id == id);
    }

    public void editarComputador(int id, String processador, int qtdRam, int qtdArmazenamento, double preco) {
        listaComputador.set(id, new Computador(id+1, processador, qtdRam, qtdArmazenamento, preco));
    }

    public double somarValores() {
        double total = 0;

        for(int i = 0; i<listaComputador.size(); i++) {
            total+=listaComputador.get(i).preco;
        }
        return total;
    }

    public double maiorValor() {
        double maior  = 0;

        for(int i = 0; i<listaComputador.size(); i++) {

            if(maior < listaComputador.get(i).preco) {
                maior = listaComputador.get(i).preco;
            }
        }

        return maior;
    }

    public double menorValor() {
        // define o primeiro valor da lista como menor inicialmente, para poder fazer as comparacoes
        double menor  = listaComputador.get(0).preco;

        for(int i = 0; i<listaComputador.size(); i++) {

            if(menor > listaComputador.get(i).preco) {
                menor = listaComputador.get(i).preco;
            }
        }

        return menor;
    }

}
