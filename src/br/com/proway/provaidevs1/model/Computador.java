package br.com.proway.provaidevs1.model;

public class Computador {

    public int id;
    public String processador;
    public int qtdRam;
    public int qtdArmazenamento;
    public double preco;

    public Computador(int id, String processador, int qtdRam, int qtdArmazenamento, double preco) {
        this.id = id;
        this.processador = processador;
        this.qtdRam = qtdRam;
        this.qtdArmazenamento = qtdArmazenamento;
        this.preco = preco;
    }

}
