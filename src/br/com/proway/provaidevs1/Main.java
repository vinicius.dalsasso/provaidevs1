package br.com.proway.provaidevs1;

import br.com.proway.provaidevs1.model.Computador;
import br.com.proway.provaidevs1.model.Dados;

import javax.swing.*;
import java.text.DecimalFormat;

public class Main {

    // usado para formatar dinheiro
    private static final DecimalFormat df = new DecimalFormat("0.00");

    public static void main (String[] args) {

        int id = 1;
        String texto;
        Dados dados =  new Dados();

        while(true) {
            texto = "                                   - Sistema de Computadores -                                   \n\n";

            for(int i = 0; i<dados.listaComputador.size(); i++) {
                texto += "Computador: " + dados.listaComputador.get(i).id + ":\nProcessador: " + dados.listaComputador.get(i).processador + ".\nQtd de Memória RAM: " + dados.listaComputador.get(i).qtdRam + " GB.\nQtd Armazenamento: " + dados.listaComputador.get(i).qtdArmazenamento + " GB.\nPreço: R$ " + df.format(dados.listaComputador.get(i).preco) + ".\n\n";
            }

            String [] opcoes = {"Adicionar", "Deletar", "Editar", "Estatísticas",  "Sair"};

            int opcao = JOptionPane.showOptionDialog(null, texto, "Computador", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, opcoes, opcoes[0]);

            if(opcao == 0) {
                JTextField processadorText = new JTextField();
                JTextField qtdRamText = new JTextField();
                JTextField qtdArmazenamentoText = new JTextField();
                JTextField precoText = new JTextField();
                Object[] message = {
                        "Processador:", processadorText,
                        "Qtd. RAM (GB):", qtdRamText,
                        "Qtd. Armazenamento (GB):", qtdArmazenamentoText,
                        "Preço(R$):", precoText
                        ,
                };
                int option = JOptionPane.showConfirmDialog(null, message, "Informe os valores", JOptionPane.OK_CANCEL_OPTION);
                if (option == JOptionPane.OK_OPTION)
                {
                    String processador = processadorText.getText();
                    int qtdRam = Integer.parseInt(qtdRamText.getText());
                    int qtdArmazenamento = Integer.parseInt(qtdArmazenamentoText.getText());
                    double preco = Double.parseDouble(precoText.getText());

                    dados.inserirComputador(new Computador(id, processador, qtdRam, qtdArmazenamento, preco));
                    id++;
                }

            } else if(opcao == 1) {
                int idComputador = Integer.parseInt(JOptionPane.showInputDialog("Informe o ID que deseja excluir"));
                dados.deletarComputador(idComputador);
            } else if(opcao == 2) {

                int idComputador = Integer.parseInt(JOptionPane.showInputDialog("Informe o ID que deseja editar"));
                idComputador--;

                JTextField processadorText = new JTextField();
                JTextField qtdRamText = new JTextField();
                JTextField qtdArmazenamentoText = new JTextField();
                JTextField precoText = new JTextField();

                processadorText.setText(dados.listaComputador.get(idComputador).processador);
                qtdRamText.setText(String.valueOf(dados.listaComputador.get(idComputador).qtdRam));
                qtdArmazenamentoText.setText(String.valueOf(dados.listaComputador.get(idComputador).qtdArmazenamento));
                precoText.setText(String.valueOf(dados.listaComputador.get(idComputador).preco));

                Object[] message = {
                        "Id:", idComputador+1,
                        "Processador:", processadorText,
                        "Qtd. RAM:", qtdRamText,
                        "Qtd. Armazenamento:", qtdArmazenamentoText,
                        "Preço:", precoText,
                };
                int option = JOptionPane.showConfirmDialog(null, message, "Edite os valores", JOptionPane.OK_CANCEL_OPTION);
                if (option == JOptionPane.OK_OPTION)
                {
                    String processador = processadorText.getText();
                    int qtdRam = Integer.parseInt(qtdRamText.getText());
                    int qtdArmazenamento = Integer.parseInt(qtdArmazenamentoText.getText());
                    double preco = Double.parseDouble(precoText.getText());
                    dados.editarComputador(idComputador, processador, qtdRam, qtdArmazenamento, preco);
                }




/*                int idComputador = Integer.parseInt(JOptionPane.showInputDialog("Informe o ID que deseja editar"));
                String processador = JOptionPane.showInputDialog("Informe o processador");
                int qtdRam = Integer.parseInt(JOptionPane.showInputDialog("Informe a qtd de memória RAM (GB)"));
                int qtdArmazenamento = Integer.parseInt(JOptionPane.showInputDialog("Informe a qtd de armazenamento (GB)"));
                double preco = Double.parseDouble(JOptionPane.showInputDialog("Informe o preço (em R$)"));*/

            }else if (opcao == 3) {
                double valorTotal = dados.somarValores();
                double menorValor = dados.menorValor();
                double maiorValor = dados.maiorValor();

                JOptionPane.showMessageDialog(null, "Temos um total de " + dados.listaComputador.size() + " computadores.\n\nNosso estoque possui um valor total de R$: " + df.format(valorTotal) + ".\nO computador mais caro em nosso estoque custa R$ "  + df.format(maiorValor) + ".\nE o de menor valor custa R$ " + df.format(menorValor) + ".");
            } else {
                break;
            }
        }

    }
}
